(function () {
    var CSS = {
        arena: {
            width: 900,
            height: 600,
            background: '#62247B',
            position: 'fixed',
            top: '50%',
            left: '50%',
            zIndex: '999',
            transform: 'translate(-50%, -50%)'
        },
        ball: {
            width: 15,
            height: 15,
            position: 'absolute',
            top: 0,
            left: 450 - 7,
            borderRadius: 50,
            background: '#C6A62F'
        },
        line: {
            width: 0,
            height: 600,
            borderLeft: '2px dashed #C6A62F',
            position: 'absolute',
            top: 0,
            left: '50%'
        },
        stick: {
            width: 12,
            height: 85,
            position: 'absolute',
            background: '#C6A62F',
            top: 260
        },
        stick1: {
            right: 0,
            top: 260
        },
        scoreBoard:{
            width:'100%',
            height: 40,
            position: 'absolute',
            top: '10%',
            zIndex: '9999',
            textAlign:'center'
        },
        scoreText:{
            width:'50%',
            lineHeight:'40px',
            fontSize:40,
            color: '#C6A62F',
            float:'left',
            height:40
        },
        winnerText:{
            width:'100%',
            height:70,
            position:'absolute',
            top:'calc(50% - 35px)',
            textAlign:'center',
            color:'#fff',
            fontSize:50,
            lineHeight:'70px'
        }
    };

    var CONSTS = {
    	gameSpeed: 10,
        score1: 0,
        score2: 0,
        stick1Speed: 0,
        stick2Speed: 0,
        ballTopSpeed: 0,
        ballLeftSpeed: 0
    };

    function start() {

        if(localStorage.getItem('score1') !== null){
            CONSTS.score1 = localStorage.getItem('score1');
        }

        if(localStorage.getItem('score2') !== null){
            CONSTS.score2 = localStorage.getItem('score2');
        }
        draw();
        setEvents();
        roll();
        loop();
    }

    function draw() {
        $('<div/>', {id: 'pong-game'}).css(CSS.arena).appendTo('body');
        $('<div/>', {id: 'pong-line'}).css(CSS.line).appendTo('#pong-game');
        $('<div/>', {id: 'pong-ball'}).css(CSS.ball).appendTo('#pong-game');
        $('<div/>', {id: 'stick-1'}).css(CSS.stick)
        .appendTo('#pong-game');
        $('<div/>', {id: 'stick-2'}).css($.extend(CSS.stick1, CSS.stick))
        .appendTo('#pong-game');
        $('<div/>', {id: 'pong-score'}).css(CSS.scoreBoard)
        .appendTo('#pong-game');
        $('<div/>', {id: 'user1'}).text(CONSTS.score1).css(CSS.scoreText).appendTo('#pong-score');
        $('<div/>', {id: 'user2'}).text(CONSTS.score2).css(CSS.scoreText).appendTo('#pong-score');
    }

    function setEvents() {
        $(document).on('keydown', function (e) {
            if (e.keyCode == 87) {
                CONSTS.stick1Speed = -11;
            }
            if (e.keyCode == 38) {
                CONSTS.stick2Speed = -11;
            }
        });

        $(document).on('keyup', function (e) {
            if (e.keyCode == 87) {
                CONSTS.stick1Speed = 0;
            }
            if (e.keyCode == 38) {
                CONSTS.stick2Speed = 0;
            }
        });
        $(document).on('keydown', function (e) {
            if (e.keyCode == 83) {
                CONSTS.stick1Speed = 11;
            }
            if (e.keyCode == 40) {
                CONSTS.stick2Speed = 11;
            }
        });

        $(document).on('keyup', function (e) {
            if (e.keyCode == 83) {
                CONSTS.stick1Speed = 0;
            }
            if (e.keyCode == 40) {
                CONSTS.stick2Speed = 0;
            }
        });
        window.addEventListener("keydown", function(e) {
            // space and arrow keys
            if([32, 37, 38, 39, 40].indexOf(e.keyCode) > -1) {
                e.preventDefault();
            }
        }, false);
    }
    var pongLoop;
    function loop() {
        pongLoop = setInterval(function () {
            save();
            collider();
            CSS.stick1.top += CONSTS.stick1Speed;
            $('#stick-1').css('top', CSS.stick1.top);
            CSS.stick.top += CONSTS.stick2Speed;
            $('#stick-2').css('top', CSS.stick.top);
            CSS.ball.top += CONSTS.ballTopSpeed;
            CSS.ball.left += CONSTS.ballLeftSpeed;

            if (CSS.ball.top <= 0 ||
                CSS.ball.top >= CSS.arena.height - CSS.ball.height) {
                CONSTS.ballTopSpeed = CONSTS.ballTopSpeed * -1;
            }

            $('#pong-ball').css({top: CSS.ball.top,left: CSS.ball.left});

            if (CSS.ball.left <= CSS.stick.width) {
            	if(!(CSS.ball.top > CSS.stick1.top && CSS.ball.top < CSS.stick1.top + CSS.stick.height && (CONSTS.ballLeftSpeed = CONSTS.ballLeftSpeed * -1))){
                    roll();
                    CONSTS.score2++;
                    $('#user2').text(CONSTS.score2.toString());
                    if(CONSTS.score2 == 5){
                        clearInterval(pongLoop);
                        winner(2);
                    }
                }
            }

            if (CSS.ball.left >= CSS.arena.width - CSS.ball.width - CSS.stick.width) {
                if(!(CSS.ball.top > CSS.stick.top && CSS.ball.top < CSS.stick.top + CSS.stick.height && (CONSTS.ballLeftSpeed = CONSTS.ballLeftSpeed * -1))){
                    roll();
                    CONSTS.score1++;
                    $('#user1').text(CONSTS.score1.toString());
                    if(CONSTS.score1 == 5){
                        clearInterval(pongLoop);
                        winner(1);
                    }
                }
            }
        }, CONSTS.gameSpeed);
    }

    function roll() {
        CSS.ball.top = 300;
        CSS.ball.left = 450;

        var side = -1;

        if (Math.random() < 0.5) {
            side = 1;
        }

        CONSTS.ballTopSpeed =  -1 * Math.random() * -2 - 3;
        CONSTS.ballLeftSpeed = side * (Math.random() * 2 + 3);
    }

    function playAgain(){ // Enter tuşu ile oyun tekrar başlar. Fonksiyon aktif edilmesi gerekmektedir.
        $(document).on('keydown', function (e) {
            if (e.keyCode == 13) {
                $('#pong-game').remove();
                start();
            }
        });
    }

    function winner(user){
        $('<div/>', {id: 'winner'}).text('USER'+user+' WINS!!').css(CSS.winnerText).appendTo('#pong-game');
        localStorage.clear();
        // CONSTS.score1 = 0;
        // CONSTS.score2 = 0;
        // playAgain();
    }

    function save(){
        $(document).on('keydown', function (e) {
            if (e.keyCode == 80) {
                localStorage.setItem('score1', CONSTS.score1);
                localStorage.setItem('score2', CONSTS.score2);
                $('<div/>', {id: 'winner'}).text('Game Saved!!').css(CSS.winnerText).appendTo('#pong-game');
                clearInterval(pongLoop);
            }
        });
    }

    function collider(){
        if(CSS.stick.top < 0)
            CSS.stick.top = 0;

        if(CSS.stick.top + CSS.stick.height > 600)
            CSS.stick.top = 515;

        if(CSS.stick1.top < 0)
            CSS.stick1.top = 0;

        if(CSS.stick1.top + CSS.stick1.height > 600)
            CSS.stick1.top = 515;
    }

    start();
})();