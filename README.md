### Rules ###
* 2 sides will play the game //ok
* Every side will have a vertical saver (stick or whatever :) width of 12px height 85px 
* There will be a 15px x 15px square ball which has a constant speed to a direction. 
* When ball hit saver or Top of the screen or  bottom of the screen the ball will be reflected like a mirror. For ex if ball hits with 30’ it will leave with 150’ (+ - 10').
* If ball reaches left or right side the ball disappears and opponent gets point.
* The ball creates in center and goes to a random direction between (30’ 150’)
* Make a scoreboard.
* 5 point wins.
* Save the game into local storage and resume on another page.
* Use W up S down for left player up arrow and down arrow for right player

### Extra Challenges ###
* Save the game when pressed p and on http://corporate.lcwaikiki.com/hakkimizda page we can contuniue from where we saved after we paste your code in console
* Make chrome extension.

### Some Extra Message(s) ###
* After one of the players wins and a message is displayed on the screen, when the enter button is pressed again, the game starts over, but the function is off because the game is constantly accelerating.
* When the game is saved and restarted, the score continues where it left off, but when one of the players wins, the memory is deleted and the game returns to the beginning.
* It is added as a Chrome extension, but it gives an error because it doesn't have jquery in itself.